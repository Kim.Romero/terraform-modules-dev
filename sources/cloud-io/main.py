import logging
import base64
import json
import os

from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google.cloud import storage
from oauth2client.service_account import ServiceAccountCredentials
import urllib.error

# Project specific variables
ACCOUNT_ID='123456'
SERVICE_ACCOUNT_EMAIL='XXX'
CUSTOM_DATA_SOURCE_ID='XXX'
WEB_PROPERTY_ID='XXX'
CREDENTIALS_KEY_FILE_LOCATION='XXX'

# Main / Entrypoint / Background Cloud Function to be triggered by Cloud Storage.
def entrypoint(data, context):
    bucket_name = data['bucket']
    obj_name = data['name']

    # Download the new bucket object from GCS
    download_to_filenamepath = "./tmp/" + obj_name
    download_gcs_file(obj_name, download_to_filenamepath, bucket_name)

    # Authenticate and construct a new Analytics service object
    analytics_service = get_service(
            api_name='analytics',
            api_version='v3',
            scopes=[],
            key_file_location=CREDENTIALS_KEY_FILE_LOCATION)

    # Send to Management API to upload the data
    upload_to_management_api(analytics_service, download_to_filenamepath)
    return

def get_service(api_name, api_version, scopes, key_file_location):
    """Get a service that communicates to a Google API.

    Args:
        api_name: The name of the api to connect to.
        api_version: The api version to connect to.
        scopes: A list auth scopes to authorize for the application.
        key_file_location: The path to a valid service account JSON key file.

    Returns:
        A service that is connected to the specified API.
    """

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            key_file_location, scopes=scopes)

    # Build the service object.
    service = build(api_name, api_version, credentials=credentials)

    return service

def upload_to_management_api(service, file_name_path):
    # This request uploads a file custom_data.csv to a particular customDataSource.
    # Note that this example makes use of the MediaFileUpload Object from the
    # apiclient.http module.
    from googleapiclient.http import MediaFileUpload

    try:
        media = MediaFileUpload(file_name_path,
                                mimetype='application/octet-stream',
                                resumable=False)
        daily_upload = service.management().uploads().uploadData(
            accountId='123456',
            webPropertyId='UA-123456-1',
            customDataSourceId='9876654321',
            media_body=media).execute()

    except TypeError as error:
        # Handle errors in constructing a query.
        print('There was an error in constructing your query : %s' % error)

    except error.HttpError as error:
        # Handle API errors.
        print('There was an API error : %s : %s' %
                (error.resp.status, error.resp.reason))

# save file to /tmp only as cloud functions only supports that to write to
def download_gcs_file(obj, to, bucket):
    client = storage.Client()
    bucket = client.get_bucket(bucket)
    blob = bucket.blob(obj)

    blob.download_to_filename(to)
    logging.debug('downloaded file {} to {}'.format(obj, to))

# needs an auth.json file as cloud auth not working for analytics requests
def get_ga_service(bucket):

    download_gcs_file('auth.json', '/tmp/auth.json', bucket)
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            '/tmp/auth.json',
            scopes=['https://www.googleapis.com/auth/analytics',
                    'https://www.googleapis.com/auth/analytics.edit'])

    # Build the service object.
    return build('analytics', 'v3', credentials=credentials, cache_discovery=False)