This directory will contains the links that will be used by the main program for cookie scraping.

"direct-links.txt"s links will be scraped directly.

"sitemaps-get-all.txt" contains sitemap URLs (found in the sitemap index) that contain largely unique URL children.

"sitemaps-get-some.txt" contains sitemap URLs (found in the sitemap index) that contain very repetitive URL children.

These three URL sources are handled differently and can be easily appended to if necessary.