provider "google" {
  project     = var.project_ID
  credentials = "key.json"
  region      = var.project_region
}

terraform {
  backend "gcs" {
    bucket  = "modules-dev-terraform-state"
    prefix  = "terraform/state"
    credentials = "key.json"
  }
    required_providers {
    google = "~> 2.20.0"
  }
}

#create a pubsub topic
resource "google_pubsub_topic" "modules_dev_docker_run" {
  name = "modules_dev_docker_run"
}


# create the storage bucket for our Cloud Function scripts
resource "google_storage_bucket" "cloudfunction-sources" {
  name     = "cloudfunction-sources-modules-dev"
  location = "EU"
  force_destroy = true
}

# create error-reporting cloud function
module "cloudfunction-error-reporting" {
  source = "./modules/error-reporting"

  source_code_bucket_name = google_storage_bucket.cloudfunction-sources.name
  environment_vars = {
    smtpPassword="hello", 
    smtpUsername="world"}
}

#create docker-run cloud function module
module "docker-run-cloudfunction" {
  source = "./modules/docker-run-cloudfunction"
  
  source_code_bucket_name = google_storage_bucket.cloudfunction-sources.name
  project_ID = var.project_ID
  pubsub_topic = "modules_dev_docker_run"
}

#create cookie-scraper module
module "cookie-scraper" {
  source = "./modules/cookie-scraper"

  project_ID = var.project_ID
  pubsub_topic = "modules_dev_pubsub_topic"
}

module "cloud-io" {
  source = "./modules/cloud-io"

  source_code_bucket_name = google_storage_bucket.cloudfunction-sources.name
  trigger_bucket_name = "modules_dev_cloud_io_trigger_bucket"
}

# create Sitespeedio / Lighthouse VM through module
module "sitespeedio" {
  source = "./modules/sitespeedio"
}