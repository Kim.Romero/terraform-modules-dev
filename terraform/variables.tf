variable "project_ID" {
    default = "relevant-online-dev"
}

variable "project_region" {
    default = "EU"
}
