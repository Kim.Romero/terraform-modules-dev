variable "source_code_bucket_name" {
  description = "Name of GCP bucket where source-code zips will be uploade into."
  type        = string
}

variable "trigger_bucket_name" {
    type        = string
    description = "Cloud storage trigger bucket."
}

locals {
    prefix = "module_dev_"
}


# zip up cloudfunction source code
data "archive_file" "cloud_io_zip" {
 type        = "zip"
 source_dir  = "../sources/cloud-io"
 output_path = "./cloud_io.zip"
}
# place the zipped code in the bucket
resource "google_storage_bucket_object" "cloud_io_zip_obj" {
 name   = "${local.prefix}cloud_io.${data.archive_file.cloud_io_zip.output_md5}.zip"
 bucket = var.source_code_bucket_name
 source = data.archive_file.cloud_io_zip.output_path
}

resource "google_storage_bucket" "cloud_io_trigger_bucket" {
    name = var.trigger_bucket_name
}

# make the cloudfunction using the above
resource "google_cloudfunctions_function" "cloud_io" {
  name                  = "${local.prefix}cloud_io"
  runtime               = "python37"
  region                = "europe-west1"  

  available_memory_mb   = 128
  source_archive_bucket = var.source_code_bucket_name
  source_archive_object = google_storage_bucket_object.cloud_io_zip_obj.name
  event_trigger {
    event_type = "google.storage.object.finalize"
    resource   = var.trigger_bucket_name
  }
  timeout               = 60
  entry_point           = "entrypoint"
}