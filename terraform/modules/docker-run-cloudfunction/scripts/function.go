// Package rundocker runs a docker image. Image is determined by the message published (recieved) by pub/sub.
package rundocker

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"google.golang.org/api/ml/v1"
)

var dockerURICookieScraper = "eu.gcr.io/" + os.Getenv("GCP_PROJECT") + "/cookie-scraper"
var dockerURISitespeedio = ""

// PubSubMessage is the payload of a Pub/Sub event.
type PubSubMessage struct {
	Data []byte `json:"data"`
}

// RunDocker consumes a Pub/Sub message and runs the appropriate docker image.
func RunDocker(ctx context.Context, m PubSubMessage) error {
	var timestamp = time.Now().Format("20060102150405")

	switch string(m.Data) {
	case "sitespeedio":
		log.Println("Starting Docker image for Sitespeed.io.")
		startJob("sitespeedio_"+timestamp, dockerURISitespeedio)
	case "cookie-scraper":
		log.Println("Starting Docker image for cookie-scraper.")
		startJob("cookiescraper_"+timestamp, dockerURICookieScraper)
	default:
		log.Println("Recieved a message not accounted for from pub/sub. Message: " + string(m.Data))
	}

	return nil
}

// Creates a new ml (machine learning) service object and
// starts a new "training job" which merely runs the docker image
// at the URI passed into this function
func startJob(jobName, dockerImgURI string) {
	ctx := context.Background()
	mlService, err := ml.NewService(ctx)
	if err != nil {
		fmt.Println(err)
	}

	var mlMasterConfig *ml.GoogleCloudMlV1__ReplicaConfig
	mlMasterConfig = new(ml.GoogleCloudMlV1__ReplicaConfig)
	mlMasterConfig.ImageUri = dockerImgURI

	var mlTrainingInput *ml.GoogleCloudMlV1__TrainingInput
	mlTrainingInput = new(ml.GoogleCloudMlV1__TrainingInput)
	mlTrainingInput.ScaleTier = "CUSTOM"
	mlTrainingInput.MasterType = "n1-standard-4"
	mlTrainingInput.Region = "europe-west1"
	mlTrainingInput.MasterConfig = mlMasterConfig

	var mlJob *ml.GoogleCloudMlV1__Job
	mlJob = new(ml.GoogleCloudMlV1__Job)
	mlJob.JobId = jobName
	mlJob.TrainingInput = mlTrainingInput

	job, err := mlService.Projects.Jobs.Create("projects/"+ os.Getenv("GCP_PROJECT"), mlJob).Do()
	if err != nil {
		fmt.Println(err, "error :(")
	}
	fmt.Printf("%+v\n", job)
}
