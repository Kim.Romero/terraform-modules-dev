module dockerrun

go 1.13

require (
	cloud.google.com/go v0.53.0 // indirect
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/api v0.19.0
	google.golang.org/genproto v0.0.0-20200302123026-7795fca6ccb1 // indirect
)
