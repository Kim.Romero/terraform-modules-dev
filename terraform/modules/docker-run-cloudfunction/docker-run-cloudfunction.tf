# variables that this module should have passed into it when used in main.tf

variable "source_code_bucket_name" {
  description = "Name of GCP bucket where source-code zips will be uploade into."
  type        = string
}

variable "project_ID" {
    type        = string
}

variable "pubsub_topic" {
    type        = string
}

locals {
    prefix = "module_dev_"
}

# zip up cloudfunction source code
data "archive_file" "docker_run_zip" {
 type        = "zip"
 source_dir  = "${path.root}/modules/docker-run-cloudfunction/scripts"
 output_path = "${path.root}/docker-run.zip"
}

# place the zipped code in the bucket
resource "google_storage_bucket_object" "docker_run_zip_obj" {
 name   = "${local.prefix}docker_run.${data.archive_file.docker_run_zip.output_md5}.zip"
 bucket = var.source_code_bucket_name
 source = data.archive_file.docker_run_zip.output_path
}

# make the cloudfunction using the above
resource "google_cloudfunctions_function" "docker_run" {
  name                  = "${local.prefix}docker_run"
  runtime               = "go111"
  region                = "europe-west1"  

  available_memory_mb   = 128
  source_archive_bucket = var.source_code_bucket_name
  source_archive_object = google_storage_bucket_object.docker_run_zip_obj.name
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/${var.project_ID}/topics/${var.pubsub_topic}"
  }
  timeout               = 500
  entry_point           = "RunDocker"
}

