variable "project_ID" {
    type        = string
}

variable "pubsub_topic" {
    type        = string
}

locals {
    prefix = "module_dev_"
}

############### Cookie Scraper as Docker image Configuration:

resource "google_cloud_scheduler_job" "modules_dev_docker_run_cookie_scraper" {
  name        = "${local.prefix}docker_run_cookie_scraper"
  region = "europe-west1"
  schedule    = "5 4 * * *"
  time_zone = "Europe/Amsterdam"
  pubsub_target {
    # topic.id is the topic's full resource name.
    topic_name = "projects/${var.project_ID}/topics/${var.pubsub_topic}"
    data       = base64encode("cookie-scraper")
  }
}

# create a dataset
resource "google_bigquery_dataset" "modules_dev_cookie_scraper_data" {
  dataset_id                  = "${local.prefix}cookie_scraper_data"
  location                    = "EU"
}
# create a table inside above dataset
resource "google_bigquery_table" "modules_dev_cookies_changes" {
  dataset_id = google_bigquery_dataset.modules_dev_cookie_scraper_data.dataset_id
  table_id   = "${local.prefix}cookies_changes"
  time_partitioning {
    type = "DAY"
  }

  schema = <<EOF
  [
    {
      "name": "Name",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "Value",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "Domain",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "Expiration",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "FoundOnHostname",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "FoundOnURL",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "Timestamp",
      "type": "STRING",
      "mode": "NULLABLE"
    },    
    {
      "name": "ChangeType",
      "type": "STRING",
      "mode": "NULLABLE"
    }
  ]
  EOF
}
# create a table inside above dataset
resource "google_bigquery_table" "modules_dev_cookies_unique" {
    dataset_id = google_bigquery_dataset.modules_dev_cookie_scraper_data.dataset_id
    table_id   = "modules_dev_cookies_unique"
    view {
    use_legacy_sql = false
    query = <<EOF
        SELECT allRecords.*
        FROM (
        SELECT Name, max(Timestamp) AS maxT
        FROM `${var.project_ID}.${google_bigquery_dataset.modules_dev_cookie_scraper_data.dataset_id}.${google_bigquery_table.modules_dev_cookies_changes.table_id}`
        Group BY Name
        ) AS selection
        INNER JOIN (
            SELECT *
            FROM `${var.project_ID}.${google_bigquery_dataset.modules_dev_cookie_scraper_data.dataset_id}.${google_bigquery_table.modules_dev_cookies_changes.table_id}`
        ) AS allRecords on selection.Name = allRecords.Name and selection.maxT = allRecords.Timestamp
        WHERE allRecords.ChangeType = 'added'
    EOF
  }
}