# variables that this module should have passed into it when used in main.tf

variable "source_code_bucket_name" {
  description = "Name of GCP bucket where source-code zips will be uploade into."
  type        = string
}

variable "environment_vars" {
  type = map
}

locals {
    prefix = "module_dev_"
}

### the making of a cloud function itself:

# zip up cloud-function source code
data "archive_file" "error_reporting_source_zip" {
 type        = "zip"
 source_dir  = "${path.root}/modules/docker-run-cloudfunction/scripts"
 output_path = "${path.root}/error-reporting.zip"
}

# place the zipped code in the bucket
resource "google_storage_bucket_object" "error_reporting_zip_bucket_obj" {
 name   = "${local.prefix}error_reporting.${data.archive_file.error_reporting_source_zip.output_md5}.zip"
 bucket = var.source_code_bucket_name
 source = data.archive_file.error_reporting_source_zip.output_path
}

# make the cloud-function using the above
resource "google_cloudfunctions_function" "error_reporting" {
  name                  = "${local.prefix}error_reporting"
  runtime               = "go111"
  region                = "europe-west1"

  available_memory_mb   = 128
  source_archive_bucket = var.source_code_bucket_name
  source_archive_object = google_storage_bucket_object.error_reporting_zip_bucket_obj.name
  trigger_http          = true
  timeout               = 500
  entry_point           = "HandleRequest"
  environment_variables = var.environment_vars
}

# create a dataset for all things GTM-monitoring related
resource "google_bigquery_dataset" "error_reporting" {
  dataset_id                  = "error_reporting"
  friendly_name               = "Error-reporting and GTM Monitoring Data"
  description                 = "Data from backend errors and GTM"
  location                    = "EU"
}

# create a table for gtm error-history inside the GTM-monitoring dataset
resource "google_bigquery_table" "error_history" {
  dataset_id = google_bigquery_dataset.error_reporting.dataset_id
  table_id   = "error_history"

  time_partitioning {
    type = "DAY"
  }

    schema = <<EOF
  [
    {
      "name": "TagName",
      "type": "STRING",
      "mode": "REQUIRED"
    },
    {
      "name": "Timestamp",
      "type": "TIMESTAMP",
      "mode": "REQUIRED"
    },
    {
      "name": "EmailSent",
      "type": "BOOLEAN",
      "mode": "REQUIRED"
    },
    {
      "name": "URL",
      "type": "STRING",
      "mode": "REQUIRED"
    }
  ]
  EOF
}