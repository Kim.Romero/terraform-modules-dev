module cloudfunction

require (
	cloud.google.com/go/bigquery v1.3.0
	google.golang.org/api v0.15.0
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)

go 1.13
