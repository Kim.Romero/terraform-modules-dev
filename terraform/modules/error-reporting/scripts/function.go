// This script converts error messages from the GTM environment to emails
// called via a try-catch construction within the GTM tags.

// Email functionality missing SMTP details!

package errorreporting

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/iterator"
	"gopkg.in/gomail.v2"
)

type Configuration struct {
	CompanyName 			string 	`json:"company_name"`
    SendToEmail 			string 	`json:"send_to_email"`
    SendFromEmail 			string 	`json:"send_from_email"`
    SendFromName 			string 	`json:"send_from_name"`
    SendFromEmailServerHost string 	`json:"send_from_email_server_host"`
    SendFromEmailServerPort int 	`json:"send_from_email_server_port"`
}

// Data structure for failed tags
type Data struct {
	URL         string `json:"url"`
	TagName     string `json:"tagName"`
	ContainerID string `json:"containerID"`
	Err         string `json:"err"`
	Timestamp   int    `json:"timestamp"`
}

// ErrorDataBigQuery structure for error data information streamed into BigQuery
type ErrorDataBigQuery struct {
	TagName   string
	EmailSent bool
	Timestamp string
	URL       string
}

// HandleRequest is the Cloud Function's entrypoint
func HandleRequest(w http.ResponseWriter, r *http.Request) {

	// decode configuration file into a struct
	file, _ := os.Open("./config.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err := decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
	}

	// Set CORS headers for the preflight request
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusNoContent)
		return
	}
	// Set CORS headers for the main request.
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// If request is of correct content-type,
	if r.Header.Get("Content-Type") == "application/json" {
		// Unmarshal request JSON to an object Go can use
		var requestData Data
		err := json.NewDecoder(r.Body).Decode(&requestData)
		if err != nil {
			log.Printf("LOG: Error parsing application/json: %v", err)
		}

		// Query error-history table to see if this error was recently reported already
		shouldSendEmail, i := queryBigQuery(requestData.TagName)
		// If it wasn't reported recently,
		if shouldSendEmail {
			// Send email
			sendMail(requestData, i)
			log.Println("LOG: Email would've been sent! Functionality only missing SMTP details. Error count in past hour: ", i)
		}
		// Add error to BigQuery error-history table
		addToBigQuery(requestData, shouldSendEmail)
	} else {
		// If request WASN'T the correct type and method, communicate that ...
		log.Printf("LOG: ERROR: request must be of type application/json and of method-type POST.")
		fmt.Fprint(w, "Request must be of type application/json and of method-type POST.")
	}
}

func sendMail(requestData Data, count int) {
	// Format timestamp
	timestampUTC := time.Unix(int64(requestData.Timestamp), 0)
	timestampUtcString := timestampUTC.Format("2006-01-02 15:04:05")

	// Format sentence depending on error count
	sentenceForCount := ""
	if count == 0 {
		sentenceForCount = "This is the first time this error occured this hour. "
	}
	sentenceForCount = "This error has occured " + strconv.Itoa(count) + " times in the past hour. "

	// Make email content string
	emailContent :=
		"Hello," + "\n\n" +
			"An error seems to have occured on " + requestData.URL + ". " +
			sentenceForCount +
			"Here is some additional information:\n\n" +
			"Tag name: " + requestData.TagName + "\n" +
			"Container ID: " + requestData.ContainerID + "\n" +
			"Timestamp: " + timestampUTC.String() + "\n\n" +
			"Regards,\n" + configuration.CompanyName + " Cloud Reporting"

	// Make gomail email object and fill
	m := gomail.NewMessage()
	m.SetBody("text/plain", emailContent)
	m.SetHeaders(map[string][]string{
		"From":    {m.FormatAddress(configuration.SendFromEmail, configuration.SendFromName)},
		"To":      {configuration.SendToEmail},
		"Subject": {"Error-Reporting Triggered"},
	})

	// Send the email using a dialer. Pass in host, port, username, and password
	d := gomail.NewPlainDialer(configuration.SendFromEmailServerHost, configuration.SendFromEmailServerPort, os.Getenv("smtpUsername"), os.Getenv("smtpPassword"))
	if err := d.DialAndSend(m); err != nil {
		log.Printf(err.Error())
	} else {
		log.Printf("LOG: Email sent!")
	}
}

func queryBigQuery(tagName string) (bool, int) {
	log.Printf("LOG: Attempting to query BigQuery...")
	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, os.Getenv("GCP_PROJECT"))

	q := client.Query(
		"SELECT * FROM gtm_monitoring.gtm_error_history " +
			"WHERE TagName='" + tagName + "' " +
			"AND Timestamp<=TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL 1 HOUR) " +
			"AND EmailSent=true")
	q.Location = "EU"
	job, err := q.Run(ctx)
	check(err)
	status, err := job.Wait(ctx)
	check(err)
	if err := status.Err(); err != nil {
		check(err)
	}

	it, err := job.Read(ctx)
	i := 0
	for {
		var row []bigquery.Value
		err := it.Next(&row)
		if err == iterator.Done {
			break
		}
		check(err)
		i++
	}
	log.Printf("LOG: Query successful! Found this same error " + strconv.Itoa(i) + " time(s) in the last hour.")

	if i > 0 {
		return false, i
	}
	return true, i
}

func addToBigQuery(data Data, haveSentEmail bool) {
    log.Printf("LOG: Attempting to add error data to BigQuery...")
	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, os.Getenv("GCP_PROJECT"))
	check(err)

	u := client.Dataset("gtm_monitoring").Table("gtm_error_history").Inserter()

	timestampUTC := time.Unix(int64(data.Timestamp)/1000, 0)
	timestampUtcString := timestampUTC.Format("2006-01-02 15:04:05")

	dataToLoad := []*ErrorDataBigQuery{
		{TagName: data.TagName, Timestamp: timestampUtcString, EmailSent: haveSentEmail, URL: data.URL},
	}
	if err := u.Put(ctx, dataToLoad); err != nil {
		if multiError, ok := err.(bigquery.PutMultiError); ok {
			for _, err1 := range multiError {
				for _, err2 := range err1.Errors {
					fmt.Println(err2)
				}
			}
		} else {
			fmt.Println(err)
		}
	}

	log.Printf("LOG: Data added to BigQuery successfully!")

}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
		fmt.Println(err)
	}
}
