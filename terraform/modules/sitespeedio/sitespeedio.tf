locals {
    prefix = "module_dev_"
}

# create VM itself running Sitespeedio
resource "google_compute_instance" "sitespeedio" {
  name         = "${local.prefix}sitespeedio-audits"
  machine_type = "n1-standard-1"
  zone         = "europe-west4-a"
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  
  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP - leaving this block empty will generate a new external IP and assign it to the machine
    }
  }

  metadata_startup_script = file("./modules/sitespeedio/scripts/startup-script.sh")
}

# create a dataset
resource "google_bigquery_dataset" "sitespeedio-data" {
  dataset_id                  = "${local.prefix}sitespeedio_data"
  location                    = "EU"
}

# create a table inside above dataset
resource "google_bigquery_table" "sitespeedio-parsed-results" {
  dataset_id = google_bigquery_dataset.sitespeedio-data.dataset_id
  table_id   = "parsed_results"

  time_partitioning {
    type = "DAY"
  }

  schema = <<EOF
  [
    {
      "name": "URL",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "Timestamp",
      "type": "TIMESTAMP",
      "mode": "NULLABLE"
    },
    {
      "name": "Requests",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "SpeedIndex",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ContentfulSpeedIndex",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "FirstPaint",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "FullyLoaded",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "VisualComplete85",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "LastVisualChange",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "OverallScore",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "PerformanceScore",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "AccessibilityScore",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "WebBestPracticeScore",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "TransferSize",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "ContentSize",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "CriticalAxeViolations",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "SeriousAxeViolations",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "MinorAxeViolations",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ModerateAxeViolations",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "LighthousePerformance",
      "type": "FLOAT",
      "mode": "NULLABLE"
    },
    {
      "name": "LighthouseAccessibility",
      "type": "FLOAT",
      "mode": "NULLABLE"
    },
    {
      "name": "LighthouseBestPractices",
      "type": "FLOAT",
      "mode": "NULLABLE"
    },
    {
      "name": "LighthouseSEO",
      "type": "FLOAT",
      "mode": "NULLABLE"
    },
    {
      "name": "FirstVisualChange",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "DOMContentLoaded",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "VisualComplete95",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "VisualComplete99",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "PerceptualSpeedIndex",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "VisualReadiness",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "BackEndTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "DOMContentLoadedTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "DOMInteractiveTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "DomainLookupTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "FrontEndTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "PageDownloadTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "PageLoadTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "RedirectionTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ServerConnectionTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ServerResponseTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "LargestContentfulPaintURL",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "LargestContentfulPaintLoadTime",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "LargestContentfulPaintSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "LighthouseOpportunities",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "JavaScriptTransferSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "JavaScriptContentSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "HTMLTransferSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "HTMLContentSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "CSSTransferSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "CSSContentSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ImageTransferSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ImageContentSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "SerializedDOMSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "DOMElements",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "AmountOfCookies",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "CookieValues",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "LocalStorageSize",
      "type": "INTEGER",
      "mode": "NULLABLE"
    },
    {
      "name": "ConsoleLog",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "Hostname",
      "type": "STRING",
      "mode": "NULLABLE"
    },
    {
      "name": "FormFactor",
      "type": "STRING",
      "mode": "NULLABLE"
    }
  ]
  EOF
}