#!/bin/sh

# Install system components
sudo apt-get update
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

# Install Docker
sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce -y

# Move files out of /tmp
sudo mkdir /etc/sitespeedio
sudo cp /tmp/volume/URLs.txt /etc/opt/sitespeedio
sudo cp /tmp/volume/key.json /etc/opt/sitespeedio

# Install Git and get Lighthouse plugin
sudo apt install git
cd /etc/opt/sitespeedio
sudo git clone https://github.com/sitespeedio/sitespeed.io.git

# Init cron and add 'docker run sitespeedio' to crontab
sudo /etc/init.d/cron start
sudo /usr/bin/crontab /tmp/volume/crontab.txt
